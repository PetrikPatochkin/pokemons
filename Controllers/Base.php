<?php
/**
 * Created by PhpStorm.
 * User: Petrik Patochkin
 * Date: 31.05.2015
 * Time: 23:55
 */
require "Models/DB.php";

class Base {
    function __construct(){
        $this->data = [];
        $this->db = new DB;
    }
}