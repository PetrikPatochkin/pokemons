<?php
/**
 * Created by PhpStorm.
 * User: Petrik Patochkin
 * Date: 31.05.2015
 * Time: 23:18
 */
require_once "Controllers/Base.php";

class Index extends Base {

    function __construct(){
        parent::__construct();
    }

    function index(){
        $quest_id = isset($_GET['question'])?(int)$_GET['question']:NULL;
        $answer_id = isset($_GET['answer'])?(int)$_GET['answer']:NULL;
        if( $quest_id AND $answer_id){
            $res = $this->db->select(
                'SELECT questions.* FROM questions LEFT JOIN quest_flow ON next_question_id=questions.id '
                .'WHERE question_id='.$quest_id.' AND answer_id='.$answer_id.' ORDER BY id LIMIT 1'
            );
        } else {
            $res = $this->db->select(
                'SELECT * FROM questions ORDER BY id LIMIT 1'
            );
        }
        $this->data['question'] = $res->fetch();

        $answers = $this->db->select(
            'SELECT id,text FROM answers LEFT JOIN quest_flow ON (answers.id=quest_flow.answer_id) '
            .'WHERE quest_flow.question_id = '.$this->data['question']['id']
        )->fetchAll();
        $this->data['answers'] = $answers;

        $this->view = 'index/index';
    }
}