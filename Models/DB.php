<?php
/**
 * Created by PhpStorm.
 * User: Petrik Patochkin
 * Date: 31.05.2015
 * Time: 23:57
 */



class DB {

    protected $db;

    function __construct(){
        require 'config.php';

        $this->db = new PDO('mysql:host=' . $config['database']['host'] . ';dbname='.$config['database']['db'].';charset=utf8',
            $config['database']['user'],$config['database']['password']);
        $this->db->exec("set names utf8");
    }

    function select($query){
        return $this->db->query($query);
    }
}