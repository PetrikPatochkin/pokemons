-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 05, 2015 at 11:24 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quest_answer`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

CREATE TABLE IF NOT EXISTS `answers` (
`id` int(11) NOT NULL,
  `text` varchar(128) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `text`) VALUES
(1, 'Для выполнения работ'),
(2, 'Для прохождения турниров'),
(3, 'Сельско-хойственные'),
(4, 'Промышленные'),
(5, 'Очень'),
(6, 'Не очень'),
(7, 'Вода'),
(8, 'Земля'),
(9, 'Огонь');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
`id` int(11) NOT NULL,
  `text` varchar(1024) NOT NULL,
  `image` varchar(256) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `text`, `image`) VALUES
(1, 'Зачем вам нужен покемон?', NULL),
(2, 'Для какого типа работ он нужен?', NULL),
(3, 'На сколько должен быть самостоятельным', NULL),
(4, 'Обязательно красивый', NULL),
(5, 'Пикачу', '/public/img/pikachu.jpg'),
(6, 'На сколько должен быть самостоятельный?', NULL),
(7, 'Раттата', '/public/img/rattata.jpg'),
(8, 'Сенд-слеш', '/public/img/sandslash.jpg'),
(9, 'Какой стихии предпочитает покемона?', NULL),
(10, 'Вульпикс', '/public/img/wulpix.jpg'),
(11, 'Недарина', '/public/img/nidorina.png'),
(12, 'Спирл', '/public/img/spirl.jpg'),
(13, 'Не удалось подобрать =(', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `quest_flow`
--

CREATE TABLE IF NOT EXISTS `quest_flow` (
  `question_id` int(11) NOT NULL,
  `answer_id` int(11) NOT NULL,
  `next_question_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `quest_flow`
--

INSERT INTO `quest_flow` (`question_id`, `answer_id`, `next_question_id`) VALUES
(1, 1, 2),
(1, 2, 9),
(2, 3, 3),
(2, 4, 6),
(3, 5, 4),
(3, 6, 13),
(4, 5, 5),
(4, 6, 13),
(6, 5, 7),
(6, 6, 8),
(9, 7, 10),
(9, 8, 11),
(9, 9, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `answers`
--
ALTER TABLE `answers`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quest_flow`
--
ALTER TABLE `quest_flow`
 ADD UNIQUE KEY `all` (`question_id`,`next_question_id`,`answer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `answers`
--
ALTER TABLE `answers`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
