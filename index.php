<?php
/**
 * Created by PhpStorm.
 * User: Petrik Patochkin
 * Date: 31.05.2015
 * Time: 22:54
 */
header('Content-Type: text/html; charset=utf-8');

if( ! isset($_GET['route'])){
    $ca =  "Index@index";
} else {
    switch($_GET['route']){
        case 'start':
            $ca = "Index@start";
            break;
        default:
            die('Page not found');
    }
}

preg_match("#(.*?)@(.*)#", $ca, $match);
$controller = $match[1];
$action = $match[2];
require_once "./Controllers/" . $controller . ".php";
require_once "./config.php";
$class =  new $controller;
$class->config = $config;
$class->$action();

$data = $class->data;
require './Views/'.$class->view.'.php';