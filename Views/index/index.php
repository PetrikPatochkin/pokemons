<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Decision</title>

    <link rel="stylesheet" href="/public/bootstrap.min.css">
    <link rel="stylesheet" href="/public/app.css"/>

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid text-center">
        <h3>Обрети свой путь и начни воевать за покемонов !!!</h3>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default main-box">
                <div class="panel-heading question text-center">
                    <?php if(is_array($data['answers']) && !empty($data['answers'])){?>
                        <?php echo $data['question']['text'];?>
                    <?php }else{?>
                        Опрос окончен

                    <?php } ?>
                </div>
                <div class="panel-body">
                    <?php if(is_array($data['answers']) && !empty($data['answers'])){?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 text-center">
                                <?php foreach($data['answers'] as $answer):?>
                                    <a class="btn btn-warning btn-answer"
                                       href="/?question=<?php echo $data['question']['id']?>&answer=<?php echo $answer['id'];?>">
                                        <?php echo $answer['text']?>
                                    </a>
                                <?php endforeach;?>
                            </div>
                        </div>
                    <?php }else{ ?>
                        <div class="row">
                            <div class="col-md-10 col-md-offset-1 text-center">
                                <div class="answer">
                                    <?php echo $data['question']['text'];?>
                                </div>
                                <?php if(isset($data['question']['image'])):?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <img  class="pokemon" src="<?php echo $data['question']['image']; ?>" alt=""/>
                                        </div>
                                    </div>
                                <?php endif;?>
                            </div>
                        </div>
                        <a class="btn btn-success"  href="/">Начать сначала</a>
                    <?php }?>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="/public/jquery.min.js"></script>
<script src="/public/bootstrap.min.js"></script>
</body>
</html>
